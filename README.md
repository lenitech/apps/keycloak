# Keycloak Helm Chart

This is a **opinionated** Helm chart for deploying [Keycloak](https://www.keycloak.org) on a [Kubernetes](https://kubernetes.io/) cluster.

It is designed to work on a [CIS](https://www.cisecurity.org/benchmark/kubernetes) constrained cluster with hardened configurations for better security.

It assume you have some dependencies already deployed on your cluster and configures deployed ressources to use them. 

## Dependencies

- **Cilium** (mandatory) :

  This chart use `CiliumNetworkPolicy.cilium.io/v2` to restrict network access **TO** and **FROM** deployed ressources, so you need to use [cilium](https://cilium.io/) as your Kubernetes [CNI](https://kubernetes.io/docs/concepts/extend-kubernetes/compute-storage-net/network-plugins/).

- **CloudNativePG** (mandatory) :

  This chart assume you use `Cluster.postgresql.cnpg.io/v1` with custom certificates and **mTLS** for needed database, so you need [cloudnative-pg](https://cloudnative-pg.io/) and a running PostgreSQL cluster.

- **Cert Manager** (mandatory) :

  This chart use `Certificate.cert-manager.io/v1` to manage **x509** certificates used for **mTLS**, so you need [cert-manager](https://cert-manager.io/) and a `ClusterIssuer.cert-manager.io/v1` named `egress`.

- **Ingress Nginx** (mandatory) :

  This chart deploy an `Ingress.networking.k8s.io/v1` with specific config and annotations, so you need [ingress-nginx](https://kubernetes.github.io/ingress-nginx/) with `nginx` as `ingressClassName`.

- **Otel Collector** (mandatory):

  This chart configure deployed ressources to send [OpenTelemetry](https://opentelemetry.io/) data to an [OTLP](https://opentelemetry.io/docs/specs/otel/protocol/) endpoint, so you need [otel-collector](https://gitlab.com/lenitech/apps/otel-collector) to receive data.

- **Smtp Relay** (optional) :

  This chart forbid any unknown outgoing traffic, so you need [smptrelay](https://gitlab.com/lenitech/apps/smtprelay) for sending any email.

- **Forecastle** (optional) :

  This chart deploy a `ForecastleApp.forecastle.stakater.com/v1alpha1` custom resources, so you cand deploy [forecastle](https://gitlab.com/lenitech/apps/forecastle) if you want to have a control panel with Keycloak in it.

- **Gatus Endpoint Operator** and **Gatus** (optional) :

  This chart deploy `GatusEndpoint.gatus.leni.tech/v1alpha1` custom resources, so you can deploy [gatus-endpoint-operator](https://gitlab.com/lenitech/apps/gatus-endpoint-operator) and [gatus](https://gitlab.com/lenitech/apps/gatus) if you want to have a status page with Keycloak in it.

## Getting started

### Helm

To install this chart with Helm:

~~~
helm install -n keycloak keycloak oci://registry.gitlab.com/lenitech/apps/keycloak
~~~

### Flux

To install this chart with Flux:

~~~
---
apiVersion: v1
kind: Namespace
metadata:
  name: keycloak
---
apiVersion: source.toolkit.fluxcd.io/v1beta2
kind: OCIRepository
metadata:
  name: keycloak
  namespace: keycloak
spec:
  interval: 1m
  url: oci://registry.gitlab.com/lenitech/apps/keycloak
---
apiVersion: helm.toolkit.fluxcd.io/v2
kind: HelmRelease
metadata:
  name: keycloak
  namespace: keycloak
spec:
  interval: 1m
  chartRef:
    kind: OCIRepository
    name: keycloak
~~~
